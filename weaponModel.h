#define WEAPON_VERTEX_COUNT 59
const S3L_Unit weaponVertices[WEAPON_VERTEX_COUNT * 3] = {
   -276,  -277,   837,        // 0
   -271,  -181,  1184,        // 3
   -479,  -281,   848,        // 6
   -180,  -187,  1172,        // 9
   -266,  -244,  1223,        // 12
   -481,  -492,   917,        // 15
   -244,  -457,   917,        // 18
   -152,  -256,  1215,        // 21
   -167,  -519,   576,        // 24
   -213,  -350,   486,        // 27
   -117,  -157,   730,        // 30
      0,  -195,   493,        // 33
      0,  -433,    70,        // 36
      0,  -624,   794,        // 39
   -120,  -766,   475,        // 42
   -337,  -570,   359,        // 45
   -284,  -661,   121,        // 48
   -254,  -420,   167,        // 51
   -348,  -478,  -235,        // 54
   -375,  -670,  -132,        // 57
      0,  -661,   121,        // 60
    -57,  -774,    82,        // 63
    -57,  -663,    71,        // 66
      0,  -670,  -131,        // 69
   -174,  -628,  -450,        // 72
      0,  -800,   135,        // 75
     57,  -774,    82,        // 78
   -236,  -476,  -236,        // 81
   -227,  -344,   -97,        // 84
   -227,  -455,   -76,        // 87
      0,  -493,  -391,        // 90
   -236,  -373,  -254,        // 93
      0,  -316,    51,        // 96
   -169,  -507,  -481,        // 99
    169,  -507,  -481,        // 102
    271,  -181,  1184,        // 105
    276,  -277,   837,        // 108
    479,  -281,   848,        // 111
    266,  -244,  1223,        // 114
    180,  -187,  1172,        // 117
    244,  -457,   917,        // 120
    152,  -256,  1215,        // 123
    167,  -519,   576,        // 126
    213,  -350,   486,        // 129
    117,  -157,   730,        // 132
    120,  -766,   475,        // 135
    337,  -570,   359,        // 138
    284,  -661,   121,        // 141
    254,  -420,   167,        // 144
    481,  -492,   917,        // 147
    348,  -478,  -235,        // 150
    375,  -670,  -132,        // 153
     57,  -663,    71,        // 156
    174,  -628,  -450,        // 159
    227,  -344,   -97,        // 162
    236,  -476,  -236,        // 165
    227,  -455,   -76,        // 168
    236,  -373,  -254,        // 171
      0,  -402,  -407         // 174
}; // weaponVertices

#define WEAPON_TRIANGLE_COUNT 114
const S3L_Index weaponTriangleIndices[WEAPON_TRIANGLE_COUNT * 3] = {
      0,     1,     2,        // 0
      3,     4,     1,        // 3
      2,     4,     5,        // 6
      6,     4,     7,        // 9
      6,     3,     0,        // 12
      8,     0,     9,        // 15
     10,     9,    11,        // 18
      9,    10,     8,        // 21
     11,     9,    12,        // 24
      0,     2,     9,        // 27
     10,    13,     8,        // 30
     13,    14,     8,        // 33
     15,    14,    16,        // 36
      2,    15,    17,        // 39
      9,     2,    17,        // 42
      5,    15,     2,        // 45
     17,    12,     9,        // 48
     15,    16,    17,        // 51
     17,    16,    18,        // 54
     18,    16,    19,        // 57
     16,    14,    20,        // 60
     20,    21,    22,        // 63
     23,    22,    21,        // 66
     24,    19,    23,        // 69
     19,    16,    22,        // 72
     16,    20,    22,        // 75
     22,    23,    19,        // 78
     21,    25,    26,        // 81
     27,    28,    29,        // 84
     30,    31,    27,        // 87
     29,    32,    12,        // 90
     17,    29,    12,        // 93
     17,    18,    29,        // 96
     29,    18,    27,        // 99
     18,    33,    27,        // 102
     27,    33,    30,        // 105
     33,    24,    34,        // 108
     15,     5,     8,        // 111
      6,     8,     5,        // 114
     14,    15,     8,        // 117
     35,    36,    37,        // 120
     38,    39,    35,        // 123
     37,    38,    35,        // 126
     38,    40,    41,        // 129
     39,    40,    36,        // 132
     36,    42,    43,        // 135
     44,    11,    43,        // 138
     43,    42,    44,        // 141
     11,    12,    43,        // 144
     36,    43,    37,        // 147
     44,    42,    13,        // 150
     13,    42,    45,        // 153
     46,    47,    45,        // 156
     37,    48,    46,        // 159
     43,    48,    37,        // 162
     49,    37,    46,        // 165
     48,    43,    12,        // 168
     46,    48,    47,        // 171
     48,    50,    47,        // 174
     51,    47,    50,        // 177
     47,    20,    45,        // 180
     20,    26,    25,        // 183
     23,    26,    52,        // 186
     53,    23,    51,        // 189
     51,    52,    47,        // 192
     47,    52,    20,        // 195
     52,    51,    23,        // 198
     21,    26,    23,        // 201
     54,    55,    56,        // 204
     57,    30,    55,        // 207
     32,    56,    12,        // 210
     48,    12,    56,        // 213
     48,    56,    50,        // 216
     56,    55,    50,        // 219
     50,    55,    34,        // 222
     55,    30,    34,        // 225
     33,    34,    30,        // 228
     46,    42,    49,        // 231
     40,    49,    42,        // 234
     45,    42,    46,        // 237
     34,    24,    53,        // 240
     24,    23,    53,        // 243
     20,    14,    45,        // 246
     14,    13,    45,        // 249
     10,    44,    13,        // 252
     10,    11,    44,        // 255
     58,    57,    31,        // 258
     31,    57,    28,        // 261
     57,    54,    28,        // 264
     28,    54,    32,        // 267
     53,    51,    50,        // 270
     19,    24,    18,        // 273
     33,    18,    24,        // 276
     34,    53,    50,        // 279
      6,     7,     3,        // 282
      8,     6,     0,        // 285
      0,     3,     1,        // 288
      3,     7,     4,        // 291
      6,     5,     4,        // 294
     35,    39,    36,        // 297
     38,    41,    39,        // 300
     37,    49,    38,        // 303
     38,    49,    40,        // 306
     39,    41,    40,        // 309
     36,    40,    42,        // 312
      2,     1,     4,        // 315
     20,    25,    21,        // 318
     27,    31,    28,        // 321
     30,    58,    31,        // 324
     29,    28,    32,        // 327
     20,    52,    26,        // 330
     54,    57,    55,        // 333
     57,    58,    30,        // 336
     32,    54,    56         // 339
}; // weaponTriangleIndices

#define WEAPON_UV_COUNT 112
const S3L_Unit weaponUVs[WEAPON_UV_COUNT * 2] = {
    136,    97,         // 0
     54,    39,         // 2
    106,   121,         // 4
     68,    25,         // 6
     39,    24,         // 8
     32,    47,         // 10
     65,   145,         // 12
    174,    52,         // 14
     21,   155,         // 16
      5,    49,         // 18
     80,     2,         // 20
     55,     5,         // 22
     64,   246,         // 24
    495,   192,         // 26
    233,   134,         // 28
    483,   343,         // 30
    196,   178,         // 32
    444,   277,         // 34
    404,   343,         // 36
      8,   503,         // 38
    305,   271,         // 40
    404,   462,         // 42
    323,   503,         // 44
    303,   271,         // 46
    411,   160,         // 48
    432,    62,         // 50
    145,   265,         // 52
    508,   123,         // 54
    176,   340,         // 56
    484,    38,         // 58
      7,   313,         // 60
    232,   276,         // 62
    281,   388,         // 64
     78,   292,         // 66
    223,   405,         // 68
      8,   377,         // 70
    407,     8,         // 72
    481,   419,         // 74
    447,   443,         // 76
    457,   433,         // 78
    482,   433,         // 80
     21,   364,         // 82
    486,   477,         // 84
    486,   361,         // 86
    447,   495,         // 88
     78,   377,         // 90
    168,   338,         // 92
    306,   460,         // 94
    435,   432,         // 96
    450,   419,         // 98
    427,   447,         // 100
    457,   404,         // 102
    303,   378,         // 104
    106,   499,         // 106
    314,    89,         // 108
     67,   472,         // 110
    281,   338,         // 112
     66,   501,         // 114
    360,   413,         // 116
    360,   413,         // 118
    166,   496,         // 120
    317,    47,         // 122
    105,   472,         // 124
    260,   128,         // 126
      8,   472,         // 128
    323,   472,         // 130
    326,   431,         // 132
    393,   450,         // 134
    326,   431,         // 136
     54,    42,         // 138
    136,    99,         // 140
    106,   123,         // 142
     40,    27,         // 144
     33,    49,         // 146
     68,    28,         // 148
     22,   156,         // 150
    173,    54,         // 152
      6,    52,         // 154
     55,     8,         // 156
     80,     5,         // 158
     65,   246,         // 160
    318,   190,         // 162
    232,   135,         // 164
    328,   341,         // 166
    195,   179,         // 168
    375,   277,         // 170
    387,    62,         // 172
    145,   265,         // 174
    309,   123,         // 176
    175,   340,         // 178
    331,    47,         // 180
      7,   442,         // 182
    231,   276,         // 184
     66,   147,         // 186
    280,   387,         // 188
     78,   462,         // 190
    221,   404,         // 192
     21,   390,         // 194
    482,   404,         // 196
    382,   486,         // 198
    168,   417,         // 200
    306,   460,         // 202
    207,    87,         // 204
    265,   472,         // 206
    301,   377,         // 208
    226,   499,         // 210
    280,   337,         // 212
    265,   501,         // 214
    205,    45,         // 216
    226,   472,         // 218
    262,     5,         // 220
    166,   472          // 222
}; // weaponUVs

#define WEAPON_UV_INDEX_COUNT 114
const S3L_Index weaponUVIndices[WEAPON_UV_INDEX_COUNT * 3] = {
      0,     1,     2,        // 0
      3,     4,     1,        // 3
      2,     5,     6,        // 6
      8,     5,     9,        // 9
      7,     3,     0,        // 12
     14,     0,    16,        // 15
     17,    15,    18,        // 18
     15,    17,    13,        // 21
     18,    15,    21,        // 24
      0,     2,    16,        // 27
     17,    24,    13,        // 30
     24,    25,    13,        // 33
     27,    25,    29,        // 36
      2,    26,    31,        // 39
     16,     2,    31,        // 42
      6,    26,     2,        // 45
     31,    20,    16,        // 48
     26,    28,    31,        // 51
     31,    28,    32,        // 54
     32,    28,    34,        // 57
     29,    25,    36,        // 60
     37,    39,    40,        // 63
     42,    40,    39,        // 66
     46,    33,    45,        // 69
     33,    30,    41,        // 72
     30,    35,    41,        // 75
     41,    45,    33,        // 78
     38,    48,    50,        // 81
     53,    55,    57,        // 84
     60,    62,    53,        // 87
     57,    64,    19,        // 90
     31,    56,    20,        // 93
     31,    32,    56,        // 96
     56,    32,    52,        // 99
     32,    66,    52,        // 102
     52,    66,    59,        // 105
     66,    47,    67,        // 108
     26,     6,    12,        // 111
      8,    12,     6,        // 114
     25,    27,    13,        // 117
     69,    70,    71,        // 120
     72,    74,    69,        // 123
     71,    73,    69,        // 126
     73,    75,    77,        // 129
     74,    76,    70,        // 132
     70,    82,    84,        // 135
     85,    18,    83,        // 138
     83,    81,    85,        // 141
     18,    21,    83,        // 144
     70,    84,    71,        // 147
     85,    81,    24,        // 150
     24,    81,    86,        // 153
     88,    90,    86,        // 156
     71,    92,    87,        // 159
     84,    92,    71,        // 162
     93,    71,    87,        // 165
     92,    84,    23,        // 168
     87,    92,    89,        // 171
     92,    94,    89,        // 174
     96,    89,    94,        // 177
     90,    36,    86,        // 180
     37,    51,    49,        // 183
     43,    51,    98,        // 186
    100,    45,    95,        // 189
     95,    97,    91,        // 192
     91,    97,    35,        // 195
     97,    95,    45,        // 198
     38,    50,    44,        // 201
    103,   105,   107,        // 204
    109,    60,   105,        // 207
     65,   107,    22,        // 210
     92,    23,   106,        // 213
     92,   106,    94,        // 216
    106,   104,    94,        // 219
     94,   104,    68,        // 222
    104,    58,    68,        // 225
     66,    67,    59,        // 228
     87,    80,    93,        // 231
     75,    93,    80,        // 234
     86,    81,    88,        // 237
     67,    47,    99,        // 240
     46,    45,   100,        // 243
     36,    25,    86,        // 246
     25,    24,    86,        // 249
     17,    85,    24,        // 252
     17,    18,    85,        // 255
    110,   108,    61,        // 258
     61,   108,    54,        // 261
    108,   102,    54,        // 264
     54,   102,    63,        // 267
    101,    96,    94,        // 270
     34,    47,    32,        // 273
     66,    32,    47,        // 276
     68,   101,    94,        // 279
      7,    10,     3,        // 282
     14,     7,     0,        // 285
      0,     3,     1,        // 288
      3,    11,     4,        // 291
      8,     6,     5,        // 294
     69,    74,    70,        // 297
     72,    78,    74,        // 300
     71,    93,    73,        // 303
     73,    93,    75,        // 306
     74,    79,    76,        // 309
     70,    76,    82,        // 312
      2,     1,     5,        // 315
     37,    49,    39,        // 318
     53,    62,    55,        // 321
     60,   111,    62,        // 324
     57,    55,    64,        // 327
     37,    98,    51,        // 330
    103,   109,   105,        // 333
    109,   111,    60,        // 336
     65,   103,   107         // 339
}; // weaponUVIndices

S3L_Model3D weaponModel;

void weaponModelInit()
{
  S3L_model3DInit(
    weaponVertices,
    WEAPON_VERTEX_COUNT,
    weaponTriangleIndices,
    WEAPON_TRIANGLE_COUNT,
    &weaponModel);
}