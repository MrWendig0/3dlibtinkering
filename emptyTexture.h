#ifndef EMPTY_TEXTURE_H
#define EMPTY_TEXTURE_H

#define EMPTY_TEXTURE_WIDTH 1
#define EMPTY_TEXTURE_HEIGHT 1

const uint8_t emptyTexture[4] = {0,0,0,0}; // level1Texture

#endif // guard
